#!/usr/bin/python
#############################################
# CMPE180A assignment - Group Project - File Store
#        Author: Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#          Date: Sep 25, 2021
# Copyright: 2021 by Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
# I certify that all code is my original work,
#  or provided by the CMPE180A instructor
# Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
#############################################

import os

from Block import Block
from utils import block_to_string, unpack_string_data, split_whitespace, hash_key


class FileSystem:
    def __init__(self, storage_file):
        """
        __init__: initializes storage file
        """
        self.storage_file = storage_file
        self.block_size = None
        self.num_blocks = None
        self.ll_head_index = None

        # used to make sure we're at most using 4 blocks in memory
        self.mem_blocks = {"header": None, "original": None, "current": None, "free": None}

    def initialize(self, block_size=None, num_blocks=None, table_to_ll_ratio=1.1):
        """
        initialize: initializes FileSystem object. Supply block_size & num_blocks if creating file system for the first time
        """
        # check if storage file exists, else create
        if not os.path.exists(self.storage_file):
            self.block_size = block_size
            self.num_blocks = num_blocks
            self.ll_head_index = self.num_blocks + 1

            # create storage file
            sf = open(self.storage_file, "wb")
            sf.close()

            # create and write header block
            self.mem_blocks["header"] = Block([self.block_size, self.num_blocks],
                                              size=32)  # as per instruction, header_block is MAX 32 bytes
            self.write_block(block_key="header")

            # Create hashtable blocks
            for i in range(self.num_blocks):
                # next iteration, table_block will point to the next Block being created; previous block wont have anything pointing to it
                self.mem_blocks["current"] = Block([], size=self.block_size)
                self.write_block()

            # Create free linkedlist blocks
            for i in range(round(self.num_blocks * table_to_ll_ratio)):
                block_index = i + self.mem_blocks["header"].data[1] + 1  # linked list begins after hash table blocks
                self.mem_blocks["current"] = Block([], size=self.block_size, pointers=[block_index + 1])
                self.write_block()
        else:
            # read header block:
            self.mem_blocks["header"] = self.read_block(0)
            block_size, num_blocks = [int(d) for d in self.mem_blocks["header"].data]
            self.block_size = block_size
            self.num_blocks = num_blocks
            self.ll_head_index = self.num_blocks + 1

    def read_block(self, index):
        """
        read_block: reads block at index. if header is not read yet, uses bs variable for block size
        """
        block_size = 32  # as per instruction, header block is MAX 32 bytes
        offset = 0
        if index != 0:
            block_size = self.block_size
            offset = 32

        ret_block = None
        with open(self.storage_file, 'rb') as g:
            g.seek(offset + (max(0, index - 1) * block_size))  # as per instruction, header block is MAX 32 bytes
            block_string = block_to_string(g.read(block_size))

            key, data, pointers = unpack_string_data(block_string)
        g.close()
        return Block(data, size=block_size, key=key, pointers=pointers)

    def find_free_block_index(self):
        """
        find_free_block_index: returns index of next free block in linked list. Opens a block but just uses it for its pointer value.
        """
        current_free_block_index = self.ll_head_index
        ll_block = self.read_block(current_free_block_index)  # read block at index
        ll_string = block_to_string(ll_block.data_bytes)

        # if out of free blocks raise exception
        if not len(ll_string.strip()):
            raise Exception(
                "Out of free blocks! Initialize dataset with more free blocks!")
        key, data, pointers = unpack_string_data(ll_string)
        self.ll_head_index = int(pointers[-1])

        ll_block = None  # clear the block from memory just in case ;)
        return current_free_block_index

    def write_block_helper(self, index, block_key="current"):
        """
        write_block_helper: writes block to index
        """
        with open(self.storage_file, 'rb+') as g:
            try:
                g.seek(32 + (max(0, index - 1) * self.block_size))  # as per instruction, header block is MAX 32 bytes
                g.write(self.mem_blocks[block_key].data_bytes)
            except:
                print("not able to write to file")
                return None

    def write_block_bst(self, index):
        """
        write_block_bst: helper fucntion to add colliding data to bst
        """
        o_key, o_data, o_pointers = unpack_string_data(block_to_string(self.mem_blocks["original"].data_bytes))
        c_key, c_data, c_pointers = unpack_string_data(block_to_string(self.mem_blocks["current"].data_bytes))

        # check if current block already has pointers (lpt,ept,gpt)
        o_lpt = "-1"
        o_ept = "-1"
        o_gpt = "-1"

        if len(o_pointers) >= 3:
            o_lpt = o_pointers[0]
            o_ept = o_pointers[1]
            o_gpt = o_pointers[2]

        propagate_block = False  # if true, progagate block down bst until it finds a place for it
        free_block_index = self.find_free_block_index()  # index to save new block to

        if c_key > o_key:
            ##if data of block to add is greater than current block's data
            if int(o_gpt) != -1:
                # if data at current index has a valid gpt, go there
                self.write_block(index=int(o_gpt))
            else:
                propagate_block = True
                o_gpt = str(free_block_index)

        elif c_key < o_key:
            ##if data of block to add is less than current block's data
            if int(o_lpt) != -1:
                # if data at current index has a valid lpt, go there
                self.write_block(index=int(o_lpt))
            else:
                propagate_block = True
                o_lpt = str(free_block_index)
        else:
            ##if data of block to add is equal to current block's data
            if int(o_ept) != -1:
                # if data at current index has a valid ept, go there
                self.write_block(index=int(o_ept))
            else:
                propagate_block = True
                o_ept = str(free_block_index)

        if propagate_block:
            if len(o_pointers) >= 3:
                o_pointers[0] = o_lpt
                o_pointers[1] = o_ept
                o_pointers[2] = o_gpt
            else:
                temp = [o_lpt, o_ept, o_gpt]
                temp.extend(o_pointers)
                o_pointers = temp

            updated_original = Block(o_data, size=self.block_size, key=o_key, pointers=o_pointers)
            self.mem_blocks["original"] = Block(o_data, size=self.block_size, key=o_key, pointers=o_pointers)
            self.write_block_helper(index,
                                    block_key="original")  # overwite block with same block but with updated pointers

            self.mem_blocks["current"] = Block(c_data, size=self.block_size, key=c_key, pointers=c_pointers)
            self.write_block(index=free_block_index)  # add (not overwrite) curr block to next free block

    def write_block(self, index=None, block_key="current"):
        """
        write_block: writes block to end of file, unless index specified
        """
        if not index:
            # append to end of file if no index specified
            storage_file = open(self.storage_file, "ab")
            storage_file.write(self.mem_blocks[block_key].data_bytes)
            storage_file.close()
        else:
            # Check if block at specified index empty
            # Else, there is a collision

            self.mem_blocks["original"] = self.read_block(index)  # read block at index
            original_string = block_to_string(self.mem_blocks["original"].data_bytes)

            o_data, empty_pts = split_whitespace(original_string)

            if len(o_data) == 0:
                # if o_data is empty, block at this index is an empty hashtable block
                self.write_block_helper(index)

            elif empty_pts:
                # get original pts values from empty linked_list block, if applicable
                # then, write block
                _, _, o_pointers = unpack_string_data(original_string)
                key, data, pointers = unpack_string_data(block_to_string(self.mem_blocks[block_key].data_bytes))
                pointers.extend(o_pointers)

                self.mem_blocks[block_key] = Block(data, size=self.block_size, key=key, pointers=pointers)
                self.write_block_helper(index, block_key)
            else:
                self.write_block_bst(index)

    def search_block(self, bst_root_block, key_to_search, bst_root_idx):
        """
        search_block: iterative BST traversal of tree at given root. Find blocks with keys equal to key_to_search
        """
        root_idx = bst_root_idx
        while bst_root_block is not None:
            r_key, r_data, r_pointers = unpack_string_data(block_to_string(self.mem_blocks[bst_root_block].data_bytes))
            r_lpt = -1
            r_ept = -1
            r_gpt = -1

            if len(r_pointers) >= 3:
                r_lpt = int(r_pointers[0])
                r_ept = int(r_pointers[1])
                r_gpt = int(r_pointers[2])

            if r_key == key_to_search:
                search_equal_idx = [root_idx]
                if r_ept != -1:
                    c_ept = r_ept
                    # go down the list of blocks which has same keys
                    while c_ept != -1:
                        search_equal_idx.append(c_ept)
                        current_equal_block = self.read_block(c_ept)
                        c_key, c_data, c_pointers = unpack_string_data(block_to_string(current_equal_block.data_bytes))

                        if len(c_pointers) >= 3:
                            c_ept = int(c_pointers[1])
                        else:
                            c_ept = -1
                    # return the list of search indexes
                    return search_equal_idx
                else:
                    return search_equal_idx
            elif r_key > key_to_search:
                root_idx = r_lpt
            else:
                root_idx = r_gpt
            self.mem_blocks[bst_root_block] = self.read_block(root_idx)

    def get_search_block_entries(self, searchEntries):
        """
        get_search_block_entries: returns data elements of blocks found in search_block()
        """
        result = []
        data_string = ""
        for entry in searchEntries:
            searchBlock = self.read_block(entry)
            data_string += "|".join([str(d) for d in searchBlock.data])
            result.append(data_string)
            data_string = ""
        return result

    def return_entries(self, key_to_search):
        """
        returns a list of call sign entries found in the fileStore at index or None if not found
        """

        searchIndex = hash_key(key_to_search, self.num_blocks)
        searchRootIdx = searchIndex  # 1  # remove this line ; only for testing
        self.mem_blocks["current"] = self.read_block(searchRootIdx)
        searchRootBlock_str = block_to_string(self.mem_blocks["current"].data_bytes).strip()
        if not len(searchRootBlock_str):
            return []

        # not empty and has non-neg1 pointers, follow tree
        searchEntries = self.search_block("current", key_to_search, searchRootIdx)

        if searchEntries is None:
            return []
        else:
            return self.get_search_block_entries(searchEntries)
