#!/usr/bin/python
#############################################
# CMPE180A assignment - Group Project - File Store
#        Author: Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#          Date: Sep 25, 2021
# Copyright: 2021 by Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
# I certify that all code is my original work,
#  or provided by the CMPE180A instructor
# Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
#############################################

def split_whitespace(data_string):
	"""
	split_whitespace: extracts non-whitespace chunks in given string
	"""
	chunks = data_string.split(" ")
	empty_pts = False  # true if data_string belongs to an empty block with pointers
	if chunks[0] == "" and chunks[len(chunks) - 1] != "":
		empty_pts = True  # only case this happens is in an empty block with pointers
	return [f for f in chunks if f != ""], empty_pts


def unpack_string_data(data_string):
	"""
	unpack_string_data: unpacks string data into Block compatible data
	"""
	key = None
	data = []
	pointers = []
	# The only time a block is entirely empty is if it is an empty
	# hash table block
	data_chunks, empty_pts = split_whitespace(data_string)
	if len(data_chunks) == 0:
		return key, data, pointers  # empty block

	if "^" in data_chunks[0]:
		key, unsp_data = data_chunks[0].split("^")
	else:
		unsp_data = data_chunks[0]

	if empty_pts:
		pointers = unsp_data.split("|")
	else:
		data = unsp_data.split("|")

	if len(data_chunks) == 2:
		pointers = data_chunks[1].split("|")

	return key, data, pointers


def block_to_string(data_bytes):
	"""
	block_to_strong: converts block data_bytes into string
	"""
	block_string = str(data_bytes)[2:-1]
	return block_string


def hash_key(key, blocks):
	"""
	hash_key: returns hash table index given a key
	"""
	# initially it is zero
	index = 0

	# mul is a multiplier initially set to 10 and calculted by	 evaluate_mul function
	mul = 10
	for i in range(0, len(key)):
		# getting mul from evaluate_multiplier()
		mul = evaluate_multiplier(mul)
		# calculating final index
		index += decimal_to_base_i(ord(key[i]), i + 2) * ((i + 1) * (mul + 1023))
	return index % blocks


def decimal_to_base_i(value, i):
	"""
	decimal_to_base_i: Converts a given decimal number to base i number
	"""
	value = int(value)
	string = ''
	while value != 0:
		string = str(value % i) + string
		value = value // i
	return int(string)


def evaluate_multiplier(mul):
	"""
	evaluate_multiplier: hash function helper (increases multiplier until it reaches a certain value, then returns it to 10
	"""
	if mul < 100000:
		mul = mul * 10
	else:
		mul = 10
	return mul
