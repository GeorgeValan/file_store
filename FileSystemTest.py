#!/usr/bin/python
#############################################
# CMPE180A assignment - Group Project - File Store
#        Author: Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#          Date: Sep 25, 2021
# Copyright: 2021 by Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
# I certify that all code is my original work,
#  or provided by the CMPE180A instructor
# Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
#############################################

from Block import Block
from FileSystem import FileSystem
from utils import unpack_string_data, hash_key


def write_entries(storage, data_to_store, limit=None):
	"""
	write_entries: Function to write the entries into the storage file

	:param storage:
	:param data_to_store:
	:param limit:
	:return:
	"""
	##1.1 Read Data from file
	indices = []  # hash table indices produced
	collision_indices = []  # indices where collision occured
	temp_indices = []  # unique hash table indices for a batch of 1000 entries
	temp_colls = []  # indices that caused collisions in a batch of 1000 entries

	# metrics files
	metrics_txt = "collision_rate.txt"
	metrics = open(metrics_txt, "w")
	metrics.write("non_collisions, collisions, total non_collisions, total collisions\n")

	ctr = 0
	with open(data_to_store, "r") as f:
		for line in f:
			line = line.strip()
			key, data, pointers = unpack_string_data(line)
			index_to_write = hash_key(key, storage.num_blocks)
			if index_to_write not in indices:
				temp_indices.append(index_to_write)
			else:
				temp_colls.append(index_to_write)

			# print("Adding: " + str(line) + " to index: " + str(index_to_write))

			data_block = Block(data, size=storage.block_size, key=key)
			storage.mem_blocks["current"] = data_block

			storage.write_block(index=index_to_write)
			if limit and ctr > limit:
				break
			ctr += 1
			if ctr % 100000 == 0:
				print("Num unique hash table indices produced: " + str(len(temp_indices)))
				print("Num indices that caused collisions: " + str(len(temp_colls)))
				indices.extend(temp_indices)
				collision_indices.extend(temp_colls)

				non_collisions = len(temp_indices)
				collisions = len(temp_colls)
				total_nc = len(indices)
				total_c = len(collision_indices)

				temp_indices = []
				temp_colls = []
				print("Total num unique hash table indices produced: " + str(len(indices)))
				print("Total num of collision indices: " + str(len(collision_indices)))
				metrics.write(
					str(non_collisions) + "," + str(collisions) + "," + str(total_nc) + "," + str(total_c) + "\n")

	print("Total no of inserts -> " + str(ctr))
	f.close()
	metrics.close()


def main():
	"""
	This is the main block to run the following

	1. Initializes the storage file
	2. Writes the data
	3. Reads the test call signs from the storage file
	"""
	data_to_store = "cmpe180projdata.txt"  # filename changed for testing
	storage_file = "storage_clean"  # This can be changed to any name of your choice
	block_size = 128
	num_blocks = 1000003  # the real number we want to use is 1000003, using shorter number for testing
	write_data = False  # Change this to True if the storage file system needs to be created again & make sure the file is not present in the folder.

	######### 0. Storage creation #########
	##0.1 Initialize storage
	print("Initializing file system:")
	storage = FileSystem(storage_file)
	storage.initialize(block_size, num_blocks)

	######### 1. File read/write ##########
	if write_data:
		write_entries(storage, data_to_store)

	testSigns = ['AD6ZH', 'K4XEN', 'AA6PM', 'W6BP', 'N2CJN', 'KA5LAV', 'KB3WAR']

	for testKey in testSigns:
		testKey = testKey.strip()
		print("Searching for key : ", testKey)

		search_idx_list = storage.return_entries(testKey)
		if search_idx_list is not None:
			print(search_idx_list)


if __name__ == "__main__":
	main()
