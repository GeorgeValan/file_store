#!/usr/bin/python
#############################################
# CMPE180A assignment - Group Project - File Store
#        Author: Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#          Date: Sep 25, 2021
# Copyright: 2021 by Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
# I certify that all code is my original work,
#  or provided by the CMPE180A instructor
# Eric Mead, Enes Yazgan, Suman Gayakwad, Gurpreet Grewal, George Valan Lourdu
#
#############################################

class Block():
    def __init__(self, data, size=32, key=None, pointers=[]):
        """
        __init__: Initialize blocks
        """
        self.data = data  # data contents
        self.size = size  # size of block
        self.key = key  # key value (if applicable)
        self.pointers = pointers  # list of pointers in a block.
        # if not empty, last pointer is ALWAYS linkedlist pointer
        # eg: [bst_lpt, bst_ept, bst_gpt, ll_fpt]
        # eg: [ll_fpt]
        # eg: []

        # convert block to bytes upon creation
        self.data_bytes = self.convert_to_bytes()

    def convert_to_bytes(self):
        """
        convert_to_bytes: convert block to bytes. used before writing a block
        """
        # convert data into string in proper format
        data_string = ""
        if self.key:
            data_string += str(self.key) + "^"

        data_string += "|".join([str(d) for d in self.data])
        pointer_string = "|".join([str(p) for p in self.pointers])

        # convert data to bytes
        data_bytes = bytes(data_string, "utf-8")
        pointer_bytes = bytes(pointer_string, "utf-8")

        # Pad rest of the block
        p_len = self.size - (len(data_bytes) + len(pointer_bytes))  # pad length

        # throw exception if data is too big for block
        if p_len <= 0:
            print("data_string -> ", data_string)
            print("data_bytes -> ", data_bytes)
            print("pointer_string -> ", pointer_string)
            print("pointer_bytes -> ", pointer_bytes)
            print("self.size -> ", str(self.size))
            print("len(data_string) -> ", str(len(data_string)))
            print("len(data_bytes) -> ", str(len(data_bytes)))
            print("len(pointer_string) -> ", str(len(pointer_string)))
            print("len(pointer_bytes) -> ", str(len(pointer_bytes)))
            raise Exception('data does not fit into block. Over by: ', str(abs(p_len)))

        data_string += " " * p_len
        data_string += pointer_string
        data_bytes = bytes(data_string, "utf-8")

        return data_bytes
